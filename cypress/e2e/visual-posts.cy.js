describe("template spec", () => {
  it("passes", () => {
    cy.visit("http://localhost:8080");
    cy.get("#navVisualPosts").click();
    cy.get("#title-input").type("Test Title");
    cy.get("#content-input").type("Test Content");
    cy.get("#addPostButton").click();
    cy.get("ul > :nth-child(101)").click();
    cy.get("form > :nth-child(3) > :nth-child(1)").click();
    cy.get("ul > :nth-child(101)").click();
    cy.get("#title-input").clear();
    cy.get("#content-input").clear();
    cy.get("#title-input").type("Updated Test Title");
    cy.get("#content-input").type("Updated Test Content");
    cy.get("form > :nth-child(3) > :nth-child(2)").click();
    cy.get("ul > :nth-child(101)").click();
    cy.get(".side-add").click();
    cy.get("#title-input").type("Second Test Title");
    cy.get("#content-input").type("Second Test Content");
    cy.get("form > :nth-child(3) > :nth-child(2)").click();
    cy.get("ul > :nth-child(101)").click();
    cy.get("form > :nth-child(3) > :nth-child(1)").click();
    cy.get("ul > :nth-child(1)").click();
    cy.get("form > :nth-child(3) > :nth-child(3)").click();
    cy.get("ul > :nth-child(1)").click();
  });
});
