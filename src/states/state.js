import { proxy } from "valtio";

export const state = proxy({
  dogList: {},
  breed: "",
  subBreed: "",
  singleImage: "",
  multiImage: [],
});
