import { LitElement, html } from "lit";
import { SingleRandomBreedImageUseCase } from "../usecases/single-random-breed-image.usecase";
import { sentenceCase } from "../utils/utils";

export class BreedUI extends LitElement {
  static get properties() {
    return {
      breed: { type: String },
      dogImage: { type: String },
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    this.dogImage = "";
    this.dogImage = await SingleRandomBreedImageUseCase.execute(this.breed);
  }

  updated(changedProperties) {
    if (changedProperties.has("breed")) {
      this.dogImage = "";
      SingleRandomBreedImageUseCase.execute(this.breed).then((result) => {
        this.dogImage = result;
      });
    }
  }

  render() {
    return html`
      <a href="/breed/${this.breed}">
        <p id="title">${sentenceCase(this.breed)}</p>
        <img
          style="width: 100px; height: 100px;"
          src="${this.dogImage.message}"
          alt=""
        />
      </a>
    `;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("breed-ui", BreedUI);
