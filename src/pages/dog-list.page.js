import { LitElement, html } from "lit";
import { AllBreedsUseCase } from "../usecases/all-breeds.usecase";
import "../ui/breed-card.ui";

export class DogList extends LitElement {
  static get properties() {
    return {
      dogList: { type: Object },
      breed: { type: String },
      page: { type: Number },
      itemsPerPage: { type: Number },
      shuffleDogList: { type: Array },
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    this.dogList = {};
    this.dogList = await AllBreedsUseCase.execute();
    this.page = 1;
    this.itemsPerPage = 10;
    this.shuffledDogList = Object.keys(this.dogList.message).sort(
      () => Math.random() - 0.5
    );
  }

  render() {
    const startIndex = (this.page - 1) * this.itemsPerPage;
    const endIndex = startIndex + this.itemsPerPage;
    const breeds = this.shuffledDogList.slice(startIndex, endIndex);

    return html`
      <h1>Lista de perros</h1>
      ${breeds.map(
        (breed, index) =>
          html`<breed-ui breed="${breed}" key="${index}"></breed-ui>`
      )}
      <div>
        <button @click="${this.handlePreviousPage}">Anterior</button>
        <button @click="${this.handleNextPage}">Siguiente</button>
      </div>
    `;
  }

  handlePreviousPage() {
    if (this.page > 1) {
      this.page -= 1;
      this.requestUpdate();
    }
  }

  handleNextPage() {
    const totalPages = Math.ceil(
      Object.keys(this?.dogList?.message).length / this.itemsPerPage
    );
    if (this.page < totalPages) {
      this.page += 1;
      this.requestUpdate();
    }
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("dog-list", DogList);
