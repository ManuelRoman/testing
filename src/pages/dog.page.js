import { LitElement, html } from "lit";
import { state } from "../states/state";
import { subscribe } from "valtio";
import { sentenceCase } from "../utils/utils";
import "../components/dog-breed.component";
import "../components/dog-subBreed.component";
import { AllSubBreedsUseCase } from "../usecases/all-subbreeds.usecase";

export class Dog extends LitElement {
  constructor() {
    super();
    subscribe(state, () => this.requestUpdate());
  }
  static get properties() {
    return {
      subBreedList: { type: Object },
    };
  }
  async connectedCallback() {
    super.connectedCallback();
    this.subBreedList = { message: [], status: "error" };
    this.subBreedList = await AllSubBreedsUseCase.execute(state.breed);
  }

  render() {
    return html`
      <h1 @click="${() => this.handleUpdateSubBreed("")}">
        ${sentenceCase(state.breed)}
      </h1>
      ${this?.subBreedList.message.length > 0
        ? html`
            <ul>
              ${this.subBreedList.message.map((subBreed) => {
                return html`<li
                  @click="${() => this.handleUpdateSubBreed(subBreed)}"
                >
                  ${sentenceCase(subBreed)}
                </li>`;
              })}
            </ul>
          `
        : null}
      ${state.subBreed === ""
        ? html`<dog-breed></dog-breed>`
        : html`<dog-subbreed></dog-subbreed>`}
    `;
  }

  handleUpdateSubBreed(subBreed) {
    state.subBreed = subBreed;
    console.log(state.subBreed);
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("dog-page", Dog);
