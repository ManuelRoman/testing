import axios from "axios";

export class DogsRepository {
  async getAllBreeds() {
    return await (
      await axios.get("https://dog.ceo/api/breeds/list/all")
    ).data;
  }

  async getSingleRandomDogImage() {
    return await (
      await axios.get("https://dog.ceo/api/breeds/image/random")
    ).data;
  }

  async getMultipleRandomDogImages(number) {
    return await (
      await axios.get(`https://dog.ceo/api/breeds/image/random/${number}`)
    ).data;
  }

  async getSingleRandomBreedImage(breed) {
    return await (
      await axios.get(`https://dog.ceo/api/breed/${breed}/images/random`)
    ).data;
  }

  async getMultipleRandomBreedImages(breed, number) {
    return await (
      await axios.get(
        `https://dog.ceo/api/breed/${breed}/images/random/${number}`
      )
    ).data;
  }

  async getAllSubBreeds(breed) {
    return await (
      await axios.get(`https://dog.ceo/api/breed/${breed}/list`)
    ).data;
  }

  async getSingleRandomSubBreedImage(breed, subBreed) {
    return await (
      await axios.get(
        `https://dog.ceo/api/breed/${breed}/${subBreed}/images/random`
      )
    ).data;
  }

  async getMultipleRandomSubBreedImages(breed, subBreed, number) {
    return await (
      await axios.get(
        `https://dog.ceo/api/breed/${breed}/${subBreed}/images/random/${number}`
      )
    ).data;
  }
}
