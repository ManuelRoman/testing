import { LitElement, html } from "lit";
import { state } from "../states/state";
import { subscribe } from "valtio";
import { sentenceCase } from "../utils/utils";
import { MultipleRandomBreedImagesUseCase } from "../usecases/multiple-random-breed-images.usecase";
import { SingleRandomBreedImageUseCase } from "../usecases/single-random-breed-image.usecase";

export class DogBreed extends LitElement {
  constructor() {
    super();
    subscribe(state, () => this.requestUpdate());
  }

  static get properties() {
    return {
      option: { type: Boolean },
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    this.option = true;
    state.singleImage = await SingleRandomBreedImageUseCase.execute(
      state.breed
    );
  }

  render() {
    return html`
      <h1>SIN SUBRAZA</h1>
      <button @click="${() => this.handleSingleImage()}">Single Image</button>
      <button @click="${() => this.handleMultipleImage()}">
        Multiple Images
      </button>
      ${this.option
        ? html`<img src="${state.singleImage.message}" />`
        : html`
            ${state.multiImage.message?.map((image) => {
              return html`<img src="${image}" />`;
            })}
          `}
    `;
  }

  async handleSingleImage() {
    this.option = true;
    state.singleImage = await SingleRandomBreedImageUseCase.execute(
      state.breed
    );
  }

  async handleMultipleImage() {
    this.option = false;
    state.multiImage = await MultipleRandomBreedImagesUseCase.execute(
      state.breed,
      3
    );
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("dog-breed", DogBreed);
