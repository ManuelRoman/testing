import { LitElement, html } from "lit";
import { state } from "../states/state";
import { subscribe } from "valtio";
import { sentenceCase } from "../utils/utils";
import { MultipleRandomSubBreedImagesUseCase } from "../usecases/multiple-random-subbreed-images.usecase";
import { SingleRandomSubBreedImageUseCase } from "../usecases/single-random-subbreed-image.usecase";

export class DogSubBreed extends LitElement {
  constructor() {
    super();
    subscribe(state, () => this.requestUpdate());
  }

  static get properties() {
    return {
      option: { type: Boolean },
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    this.option = true;
    state.singleImage = await SingleRandomSubBreedImageUseCase.execute(
      state.breed,
      state.subBreed
    );
  }

  render() {
    return html`
      <h1>SIN SUBRAZA</h1>
      <button @click="${() => this.handleSingleImage()}">Single Image</button>
      <button @click="${() => this.handleMultipleImage()}">
        Multiple Images
      </button>
      ${this.option
        ? html`<img src="${state.singleImage.message}" />`
        : html`
            ${state.multiImage.message.map((image) => {
              return html`<img src="${image}" />`;
            })}
          `}
    `;
  }

  async handleSingleImage() {
    this.option = true;
    state.singleImage = await SingleRandomSubBreedImageUseCase.execute(
      state.breed,
      state.subBreed
    );
  }

  async handleMultipleImage() {
    this.option = false;
    state.multiImage = await MultipleRandomSubBreedImagesUseCase.execute(
      state.breed,
      state.subBreed,
      3
    );
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("dog-subbreed", DogSubBreed);
