import { DogsRepository } from "../repositories/dogs.repository";

export class MultipleRandomSubBreedImagesUseCase {
  static async execute(breed, subBreed, number) {
    const repository = new DogsRepository();
    return await repository.getMultipleRandomSubBreedImages(
      breed,
      subBreed,
      number
    );
  }
}
