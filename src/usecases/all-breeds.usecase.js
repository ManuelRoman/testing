import { DogsRepository } from "../repositories/dogs.repository";

export class AllBreedsUseCase {
  static async execute() {
    const repository = new DogsRepository();
    return await repository.getAllBreeds();
  }
}
