import { Router } from "@vaadin/router";
import { state } from "./states/state";
import "./main.css";
import "./pages/home.page";
import "./pages/dog-list.page";
import "./pages/dog.page.js";
// import "./pages/cosa.page";

// Home, lista de razas, cada raza + subrazas

const outlet = document.querySelector("#outlet");
const router = new Router(outlet);

router.setRoutes([
  {
    path: "/",
    component: "home-page",
    action: () => {
      state.breed = "";
      state.subBreed = "";
    },
  },
  {
    path: "/breed/list",
    component: "dog-list",
    action: () => {
      state.breed = "";
      state.subBreed = "";
    },
  },
  {
    path: "/breed/:breedName",
    component: "dog-page",
    action: (context) => {
      state.breed = context.params.breedName;
    },
  },
  { path: "(.*)", redirect: "/" },
]);
