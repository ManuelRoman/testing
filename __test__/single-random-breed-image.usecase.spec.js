import { DogsRepository } from "../src/repositories/dogs.repository";
import { SingleRandomBreedImageUseCase } from "../src/usecases/single-random-breed-image.usecase";
import { RANDOM_BREED_IMAGE } from "./fixtures/singleRandomBreedImage";

jest.mock("../src/repositories/dogs.repository");

describe("Get single random dog breed image", () => {
  beforeEach(() => {
    DogsRepository.mockClear();
  });

  it("Should run", async () => {
    DogsRepository.mockImplementation(() => {
      return {
        getSingleRandomBreedImage: () => {
          return RANDOM_BREED_IMAGE;
        },
      };
    });

    const breed = "hound";

    const singleBreedImage = await SingleRandomBreedImageUseCase.execute(breed);

    expect(singleBreedImage.status).toBe(RANDOM_BREED_IMAGE.status);

    expect(singleBreedImage.message).toBe(RANDOM_BREED_IMAGE.message);
  });
});
