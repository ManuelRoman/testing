import { DogsRepository } from "../src/repositories/dogs.repository";
import { MultipleRandomSubBreedImagesUseCase } from "../src/usecases/multiple-random-subbreed-images.usecase";
import { RANDOM_SUBBREED_IMAGES } from "./fixtures/multipleRandomSubBreedImages";

jest.mock("../src/repositories/dogs.repository");

describe("Get multiple random dog breed images", () => {
  beforeEach(() => {
    DogsRepository.mockClear();
  });

  it("Should run", async () => {
    DogsRepository.mockImplementation(() => {
      return {
        getMultipleRandomSubBreedImages: () => {
          return RANDOM_SUBBREED_IMAGES;
        },
      };
    });

    const breed = "hound";
    const subBreed = "afghan";
    const number = 3;

    const multipleBreedImages =
      await MultipleRandomSubBreedImagesUseCase.execute(
        breed,
        subBreed,
        number
      );

    console.log(multipleBreedImages);

    expect(multipleBreedImages.status).toBe(RANDOM_SUBBREED_IMAGES.status);

    expect(multipleBreedImages.message[0]).toBe(
      RANDOM_SUBBREED_IMAGES.message[0]
    );

    expect(multipleBreedImages.message.length).toBe(
      RANDOM_SUBBREED_IMAGES.message.length
    );
  });
});
